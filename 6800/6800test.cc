#include <sstream>
#include <iostream>
#include <iomanip>
#include <set>

using namespace std;

extern "C" {
  #include "6800.h"
};

extern uint8_t dreamROM[1024];

uint8_t g_ram[1024];

uint8_t ReadMem(uint16_t addr)
{
	//cout << "     read 0x" << setw(4) << setfill('0') << hex << addr << endl; 
  if (addr < 0x400)
    return g_ram[addr & 0x3ff];
  if (addr >= 0xc000)
    return dreamROM[addr & 0x3ff];
  return 0;  
}

void WriteMem(uint16_t addr, uint8_t data)
{
	//cout << "     write 0x" << setw(4) << setfill('0') << hex << addr << endl; 
  if (addr < 0x400)
    g_ram[addr & 0x3ff] = data;
}

void Trap(uint16_t addr, uint8_t data)
{
//  cout << "unknown blah opcode 0x" << hex << (int)data << " at 0x" << hex << addr << endl; 
}

int main(int argc, char *argv[])
{
  M6800 cpu;

  M6800_Init(&cpu);

  cpu.m_trap      = &Trap;
  cpu.m_readMem   = &ReadMem;
  cpu.m_writeMem  = &WriteMem;

  for (;;) {
    M6800_Run(&cpu, 1);
    cout << setw(4) << setfill('0') << hex << cpu.m_insAddr << "  ";
    int i;
    int len = cpu.m_insLen;
    if (len == 0)
      len = 1;
    for (i = 0; i < len; ++i)
      cout << " " << setw(2) << setfill('0') << hex << (int)cpu.m_ins[i];
    if (cpu.m_insLen > 0) {
      for (; i < 4; ++i)
        cout << "   ";
			char text[20];
			M6800_Disassemble(cpu.m_ins, cpu.m_insLen, text, sizeof(text));	
      cout << text;  
    }
    cout << endl; 
  }

  return 0;
}	



/* C:\Users\craig\Dropbox\Vintage Tech\Dream6800_40th_Anniv\40th Anniversary\ROM\Dream6800Rom.bin (30/10/2008 10:43:22 AM)
   StartOffset(h): 00000000, EndOffset(h): 000003FF, Length(h): 00000400 */

uint8_t dreamROM[1024] = {
	0x8D, 0x77, 0xCE, 0x02, 0x00, 0xDF, 0x22, 0xCE, 0x00, 0x5F, 0xDF, 0x24,
	0xDE, 0x22, 0xEE, 0x00, 0xDF, 0x28, 0xDF, 0x14, 0xBD, 0xC0, 0xD0, 0x96,
	0x14, 0x84, 0x0F, 0x97, 0x14, 0x8D, 0x21, 0x97, 0x2E, 0xDF, 0x2A, 0x96,
	0x29, 0x44, 0x44, 0x44, 0x44, 0x8D, 0x15, 0x97, 0x2F, 0xCE, 0xC0, 0x48,
	0x96, 0x28, 0x84, 0xF0, 0x08, 0x08, 0x80, 0x10, 0x24, 0xFA, 0xEE, 0x00,
	0xAD, 0x00, 0x20, 0xCC, 0xCE, 0x00, 0x2F, 0x08, 0x4A, 0x2A, 0xFC, 0xA6,
	0x00, 0x39, 0xC0, 0x6A, 0xC0, 0xA2, 0xC0, 0xAC, 0xC0, 0xBA, 0xC0, 0xC1,
	0xC0, 0xC8, 0xC0, 0xEE, 0xC0, 0xF2, 0xC0, 0xFE, 0xC0, 0xCC, 0xC0, 0xA7,
	0xC0, 0x97, 0xC0, 0xF8, 0xC2, 0x1F, 0xC0, 0xD7, 0xC1, 0x5F, 0xD6, 0x28,
	0x26, 0x25, 0x96, 0x29, 0x81, 0xE0, 0x27, 0x05, 0x81, 0xEE, 0x27, 0x0E,
	0x39, 0x4F, 0xCE, 0x01, 0x00, 0xA7, 0x00, 0x08, 0x8C, 0x02, 0x00, 0x26,
	0xF8, 0x39, 0x30, 0x9E, 0x24, 0x32, 0x97, 0x22, 0x32, 0x97, 0x23, 0x9F,
	0x24, 0x35, 0x39, 0xDE, 0x14, 0x6E, 0x00, 0x96, 0x30, 0x5F, 0x9B, 0x15,
	0x97, 0x15, 0xD9, 0x14, 0xD7, 0x14, 0xDE, 0x14, 0xDF, 0x22, 0x39, 0xDE,
	0x14, 0xDF, 0x26, 0x39, 0x30, 0x9E, 0x24, 0x96, 0x23, 0x36, 0x96, 0x22,
	0x36, 0x9F, 0x24, 0x35, 0x20, 0xE8, 0x96, 0x29, 0x91, 0x2E, 0x27, 0x10,
	0x39, 0x96, 0x29, 0x91, 0x2E, 0x26, 0x09, 0x39, 0x96, 0x2F, 0x20, 0xF0,
	0x96, 0x2F, 0x20, 0xF3, 0xDE, 0x22, 0x08, 0x08, 0xDF, 0x22, 0x39, 0xBD,
	0xC2, 0x97, 0x7D, 0x00, 0x18, 0x27, 0x07, 0xC6, 0xA1, 0xD1, 0x29, 0x27,
	0xEB, 0x39, 0xC6, 0x9E, 0xD1, 0x29, 0x27, 0xD0, 0x20, 0xD5, 0x96, 0x29,
	0x20, 0x3B, 0x96, 0x29, 0x9B, 0x2E, 0x20, 0x35, 0x8D, 0x38, 0x94, 0x29,
	0x20, 0x2F, 0x96, 0x2E, 0xD6, 0x29, 0xC4, 0x0F, 0x26, 0x02, 0x96, 0x2F,
	0x5A, 0x26, 0x02, 0x9A, 0x2F, 0x5A, 0x26, 0x02, 0x94, 0x2F, 0x5A, 0x5A,
	0x26, 0x0A, 0x7F, 0x00, 0x3F, 0x9B, 0x2F, 0x24, 0x03, 0x7C, 0x00, 0x3F,
	0x5A, 0x26, 0x0A, 0x7F, 0x00, 0x3F, 0x90, 0x2F, 0x25, 0x03, 0x7C, 0x00,
	0x3F, 0xDE, 0x2A, 0xA7, 0x00, 0x39, 0x86, 0xC0, 0x97, 0x2C, 0x7C, 0x00,
	0x2D, 0xDE, 0x2C, 0x96, 0x0D, 0xAB, 0x00, 0xA8, 0xFF, 0x97, 0x0D, 0x39,
	0x07, 0xC1, 0x79, 0x0A, 0xC1, 0x7D, 0x15, 0xC1, 0x82, 0x18, 0xC1, 0x85,
	0x1E, 0xC1, 0x89, 0x29, 0xC1, 0x93, 0x33, 0xC1, 0xDE, 0x55, 0xC1, 0xFA,
	0x65, 0xC2, 0x04, 0xCE, 0xC1, 0x44, 0xC6, 0x09, 0xA6, 0x00, 0x91, 0x29,
	0x27, 0x09, 0x08, 0x08, 0x08, 0x5A, 0x26, 0xF4, 0x7E, 0xC3, 0x60, 0xEE,
	0x01, 0x96, 0x2E, 0x6E, 0x00, 0x96, 0x20, 0x20, 0xB0, 0xBD, 0xC2, 0xC4,
	0x20, 0xAB, 0x97, 0x20, 0x39, 0x16, 0x7E, 0xC2, 0xE1, 0x5F, 0x9B, 0x27,
	0x97, 0x27, 0xD9, 0x26, 0xD7, 0x26, 0x39, 0xCE, 0xC1, 0xBC, 0x84, 0x0F,
	0x08, 0x08, 0x4A, 0x2A, 0xFB, 0xEE, 0x00, 0xDF, 0x1E, 0xCE, 0x00, 0x08,
	0xDF, 0x26, 0xC6, 0x05, 0x96, 0x1E, 0x84, 0xE0, 0xA7, 0x04, 0x09, 0x86,
	0x03, 0x79, 0x00, 0x1F, 0x79, 0x00, 0x1E, 0x4A, 0x26, 0xF7, 0x5A, 0x26,
	0xEB, 0x39, 0xF6, 0xDF, 0x49, 0x25, 0xF3, 0x9F, 0xE7, 0x9F, 0x3E, 0xD9,
	0xE7, 0xCF, 0xF7, 0xCF, 0x24, 0x9F, 0xF7, 0xDF, 0xE7, 0xDF, 0xB7, 0xDF,
	0xD7, 0xDD, 0xF2, 0x4F, 0xD6, 0xDD, 0xF3, 0xCF, 0x93, 0x4F, 0xDE, 0x26,
	0xC6, 0x64, 0x8D, 0x06, 0xC6, 0x0A, 0x8D, 0x02, 0xC6, 0x01, 0xD7, 0x0E,
	0x5F, 0x91, 0x0E, 0x25, 0x05, 0x5C, 0x90, 0x0E, 0x20, 0xF7, 0xE7, 0x00,
	0x08, 0x39, 0x0F, 0x9F, 0x12, 0x8E, 0x00, 0x2F, 0xDE, 0x26, 0x20, 0x09,
	0x0F, 0x9F, 0x12, 0x9E, 0x26, 0x34, 0xCE, 0x00, 0x30, 0xD6, 0x2B, 0xC4,
	0x0F, 0x32, 0xA7, 0x00, 0x08, 0x7C, 0x00, 0x27, 0x5A, 0x2A, 0xF6, 0x9E,
	0x12, 0x0E, 0x39, 0xD6, 0x29, 0x7F, 0x00, 0x3F, 0xDE, 0x26, 0x86, 0x01,
	0x97, 0x1C, 0xC4, 0x0F, 0x26, 0x02, 0xC6, 0x10, 0x37, 0xDF, 0x14, 0xA6,
	0x00, 0x97, 0x1E, 0x7F, 0x00, 0x1F, 0xD6, 0x2E, 0xC4, 0x07, 0x27, 0x09,
	0x74, 0x00, 0x1E, 0x76, 0x00, 0x1F, 0x5A, 0x26, 0xF5, 0xD6, 0x2E, 0x8D,
	0x28, 0x96, 0x1E, 0x8D, 0x15, 0xD6, 0x2E, 0xCB, 0x08, 0x8D, 0x1E, 0x96,
	0x1F, 0x8D, 0x0B, 0x7C, 0x00, 0x2F, 0xDE, 0x14, 0x08, 0x33, 0x5A, 0x26,
	0xCB, 0x39, 0x16, 0xE8, 0x00, 0xAA, 0x00, 0xE7, 0x00, 0x11, 0x27, 0x04,
	0x86, 0x01, 0x97, 0x3F, 0x39, 0x96, 0x2F, 0x84, 0x1F, 0x48, 0x48, 0x48,
	0xC4, 0x3F, 0x54, 0x54, 0x54, 0x1B, 0x97, 0x1D, 0xDE, 0x1C, 0x39, 0xC6,
	0xF0, 0xCE, 0x80, 0x10, 0x6F, 0x01, 0xE7, 0x00, 0xC6, 0x06, 0xE7, 0x01,
	0x6F, 0x00, 0x39, 0x8D, 0xEE, 0x7F, 0x00, 0x18, 0x8D, 0x55, 0xE6, 0x00,
	0x8D, 0x15, 0x97, 0x17, 0xC6, 0x0F, 0x8D, 0xE1, 0xE6, 0x00, 0x54, 0x54,
	0x54, 0x54, 0x8D, 0x07, 0x48, 0x48, 0x9B, 0x17, 0x97, 0x17, 0x39, 0xC1,
	0x0F, 0x26, 0x02, 0xD7, 0x18, 0x86, 0xFF, 0x4C, 0x54, 0x25, 0xFC, 0x39,
	0xDF, 0x12, 0x8D, 0xBF, 0xA6, 0x01, 0x2B, 0x07, 0x48, 0x2A, 0xF9, 0x6D,
	0x00, 0x20, 0x07, 0x8D, 0xC2, 0x7D, 0x00, 0x18, 0x26, 0xEC, 0x8D, 0x03,
	0xDE, 0x12, 0x39, 0xC6, 0x04, 0xD7, 0x21, 0xC6, 0x41, 0xF7, 0x80, 0x12,
	0x7D, 0x00, 0x21, 0x26, 0xFB, 0xC6, 0x01, 0xF7, 0x80, 0x12, 0x39, 0x8D,
	0x00, 0x37, 0xC6, 0xC8, 0x5A, 0x01, 0x26, 0xFC, 0x33, 0x39, 0xCE, 0x80,
	0x12, 0xC6, 0x3B, 0xE7, 0x01, 0xC6, 0x7F, 0xE7, 0x00, 0xA7, 0x01, 0xC6,
	0x01, 0xE7, 0x00, 0x39, 0x8D, 0x13, 0xA6, 0x00, 0x2B, 0xFC, 0x8D, 0xDD,
	0xC6, 0x09, 0x0D, 0x69, 0x00, 0x46, 0x8D, 0xD3, 0x5A, 0x26, 0xF7, 0x20,
	0x17, 0xDF, 0x12, 0xCE, 0x80, 0x12, 0x39, 0x8D, 0xF8, 0x36, 0x6A, 0x00,
	0xC6, 0x0A, 0x8D, 0xBF, 0xA7, 0x00, 0x0D, 0x46, 0x5A, 0x26, 0xF7, 0x32,
	0xDE, 0x12, 0x39, 0x20, 0x83, 0x86, 0x37, 0x8D, 0xB9, 0xDE, 0x02, 0x39,
	0x8D, 0xF7, 0xA6, 0x00, 0x8D, 0xDD, 0x08, 0x9C, 0x04, 0x26, 0xF7, 0x20,
	0x0B, 0x8D, 0xEA, 0x8D, 0xB7, 0xA7, 0x00, 0x08, 0x9C, 0x04, 0x26, 0xF7,
	0x8E, 0x00, 0x7F, 0xCE, 0xC3, 0xE9, 0xDF, 0x00, 0x86, 0x3F, 0x8D, 0x92,
	0x8D, 0x43, 0x0E, 0x8D, 0xCE, 0x4D, 0x2A, 0x10, 0x8D, 0xC9, 0x84, 0x03,
	0x27, 0x23, 0x4A, 0x27, 0xD8, 0x4A, 0x27, 0xC8, 0xDE, 0x06, 0x6E, 0x00,
	0x8D, 0x0C, 0x97, 0x06, 0x8D, 0x06, 0x97, 0x07, 0x8D, 0x23, 0x20, 0xDF,
	0x8D, 0xAD, 0x48, 0x48, 0x48, 0x48, 0x97, 0x0F, 0x8D, 0xA5, 0x9B, 0x0F,
	0x39, 0x8D, 0x12, 0xDE, 0x06, 0x8D, 0x25, 0x8D, 0x9A, 0x4D, 0x2B, 0x04,
	0x8D, 0xE8, 0xA7, 0x00, 0x08, 0xDF, 0x06, 0x20, 0xEC, 0x86, 0x10, 0x8D,
	0x2B, 0xCE, 0x01, 0xC8, 0x86, 0xFF, 0xBD, 0xC0, 0x7D, 0xCE, 0x00, 0x06,
	0x8D, 0x06, 0x08, 0x8D, 0x03, 0x8D, 0x15, 0x39, 0xA6, 0x00, 0x36, 0x44,
	0x44, 0x44, 0x44, 0x8D, 0x01, 0x32, 0xDF, 0x12, 0xBD, 0xC1, 0x93, 0xC6,
	0x05, 0xBD, 0xC2, 0x24, 0x86, 0x04, 0x9B, 0x2E, 0x97, 0x2E, 0x86, 0x1A,
	0x97, 0x2F, 0xDE, 0x12, 0x39, 0x7A, 0x00, 0x20, 0x7A, 0x00, 0x21, 0x7D,
	0x80, 0x12, 0x3B, 0xDE, 0x00, 0x6E, 0x00, 0x00, 
	0xC3, 0xF3,   // irq vector
	0x00, 0x80,   // software interrupt
	0x00, 0x83,   // nmi vector
	0xC3, 0x60    // reset vector
};

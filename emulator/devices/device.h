
#ifndef VIRTUALDEVICE_H_
#define VIRTUALDEVICE_H_

class VirtualDevice
{
  public:
    virtual void Reset() = 0;
};

#endif // VIRTUALDEVICE_H_


#ifndef KEYPAR_H_
#define KEYPAR_H_

#include <map>
#include <vector>
#include <functional>

#include "devices/keyboard.h"

class ParallelKeyboard : public VirtualKeyboard
{
  public:
    struct Mapping {
      bool m_defaultUpper   = true;
      bool m_arrowsToWASD   = true;
      bool m_bsToDelete     = true;
      bool m_deleteToBs     = false;
      bool m_shiftEnterToLF = true;
    };

    ParallelKeyboard();
    ParallelKeyboard(const Mapping & mapping);

    virtual void OnKeyChar(char ch);

  protected:    
    Mapping m_mapping;
};

#endif // KEYPAR_H_
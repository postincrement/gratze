
#ifndef VIRTUALSCREEN_H_
#define VIRTUALSCREEN_H_

#include <vector>
#include <map>
#include <memory>
#include <chrono>
#include <map>
#include <set>
#include <functional>

#include <SDL.h>

#include "common/factory.h"
#include "src/options.h"
#include "video/font.h"

class MainWindow;

namespace Config {
  class Video;
}

class VirtualScreen
{
  public:
    VirtualScreen(const Options & options, int cols, int rows);
    virtual ~VirtualScreen();
 
    virtual bool Open();
    virtual int GetRows() const;
    virtual int GetCols() const;

    virtual void SetCharAtLoc(int loc, FontChar ch);
    virtual FontChar GetCharAtLoc(int addr) const;

    virtual void SetColourAtLoc(int loc, const SDL_Colour & fg, const SDL_Colour & bg);
    virtual void GetColourAtLoc(int loc, SDL_Colour & fg, SDL_Colour & bg);

    virtual int MapPosToLoc(int x, int y);
    virtual bool MapLocToPos(int & x, int & y, int addr);
    
    virtual FontChar GetCharAtPos(int x, int y);
    virtual void GetColourAtPos(int x, int y, SDL_Colour & fg, SDL_Colour & bg);

    virtual void RefreshCharAtLoc(int loc, bool update = true);
    virtual void RefreshScreen();

    virtual void SetScale(int hscale, int vscale);
    virtual void SetColScale(int scale);

    virtual void EnableCursor(bool enable = true);
    virtual void SetCursorPos(int x, int y);
    virtual void Update(bool hasChanged = false);

    virtual void OnUpdate() = 0;
    virtual bool ResizeScreen() = 0;

    virtual bool SetFont(Font * font, int cols = -1, int rows = -1) = 0;

    virtual void RenderCharAtPos(int x, int y, bool withCursor, bool update = true) = 0;

    struct CharCell
    {
      CharCell()
        : m_ch(' ')
      { }
      CharCell(uint8_t ch)
        : m_ch(ch)
      { }
      FontChar  m_ch;
      SDL_Color m_fg;
      SDL_Color m_bg;
    };

  protected:
    Options m_options;
    int m_rows;
    int m_cols;
    SDL_Color m_fg;
    SDL_Color m_bg;

    int m_visibleSize = 0;
    int m_visibleMask = 0;

    int m_hscale = 1;
    int m_vscale = 1;
    int m_colScale = 1;

    bool m_softCursor = false;
    int m_cursorX = 0;
    int m_cursorY = 0;
    bool m_cursorEnabled = false;

    std::vector<CharCell> m_chars;
};

class SDLVirtualScreen : public VirtualScreen
{
  public:
    SDLVirtualScreen(MainWindow & mainWindow, const Options & options, int cols, int rows);

    virtual void RenderCharAtPos(int x, int y, bool withCursor, bool update = true) override;
    virtual void Update(bool hasChanged = false) override;
    virtual void OnUpdate() override;
    virtual bool SetFont(Font * font, int cols = -1, int rows = -1) override;
    virtual bool ResizeScreen() override;

  private:  
    virtual void RenderChar(FontChar ch, bool withCursor, SDL_Renderer * renderer, const SDL_Rect & dstRect, const SDL_Colour & fg, const SDL_Colour & bg);

  protected:
    MainWindow & m_mainWindow;

    int m_width;
    int m_height;

    bool m_lazyUpdates;
    bool m_dirty;
    std::chrono::system_clock::time_point m_updateTimer;
    std::unique_ptr<Font> m_font;
};

/////////////////////////////////////////////////////////////////////////////////

class MemoryMappedScreen;

using MemoryMappedScreenFactory = Factory<MemoryMappedScreen, std::string, MainWindow &, const Options &, const Config::MemoryMappedScreen &>;

class MemoryMappedScreen : public SDLVirtualScreen
{
  public:
    MemoryMappedScreen(MainWindow & mainWindow, const Options & options, const Config::MemoryMappedScreen & info);
    virtual ~MemoryMappedScreen();

    // CPU access
    virtual void WriteMemoryAtAddress(int addr, uint8_t ch);
    virtual uint8_t ReadMemoryAtAddress(int addr) const;

    // overrides from VirtualScreen
    virtual void GetColourAtLoc(int loc, SDL_Colour & fg, SDL_Colour & bg) override = 0;
    virtual FontChar GetCharAtLoc(int loc) const override;
    virtual void OnUpdate() override;
    virtual bool SetFont(Font * font, int cols = -1, int rows = -1) override;

    // new functions
    virtual void SetFontColour(const SDL_Colour & fg, const SDL_Colour & bg) = 0;
    virtual void GetFontColour(SDL_Colour & fg, SDL_Colour & bg) const = 0;

    virtual void SetOffset(uint16_t offset);

    static MemoryMappedScreen * Create(MainWindow & mainWindow, const Options & options, const Config::MemoryMappedScreen & info);

    template<class Type>
    static void AddType(const std::string & name)
    {
      if (!g_memoryMappedScreenFactory.HasKey(name))
        g_memoryMappedScreenFactory.AddConcreteClass<Type>(name);
    }

    virtual void InitPCG();
    virtual void UpdatePCG(int loc, FontChar oldChar, FontChar newChar);

    virtual bool IsPCG(FontChar ch) const;
    virtual void SetPCG(FontChar ch, int line, uint8_t val);
    virtual void SetPCG(FontChar ch, int row, int rowCount, uint8_t * val);
    virtual void RewritePCG();

    virtual void SetRewritePCGHandler(std::function<void ()> handler);

    struct CharUsage
    {
      std::set<int> m_locs;
    };

  protected:
    std::vector<uint8_t> m_memory;
    int m_memoryMask = 0;
    int m_offset;
    static MemoryMappedScreenFactory g_memoryMappedScreenFactory;

    bool m_hasPCG = false;
    std::map<FontChar, CharUsage> m_pcgLocs;
    std::set<FontChar> m_pcgDirty;
    std::function<void ()> m_rewritePCGHandler = nullptr;
};

/////////////////////////////////////////////////////////////////////////////////

class MonoMemoryMappedScreen : public MemoryMappedScreen
{
  public:
    MonoMemoryMappedScreen(MainWindow & mainWindow, const Options & options, const Config::MemoryMappedScreen & info);

    virtual void SetFontColour(const SDL_Colour & fg, const SDL_Colour & bg);
    virtual void GetFontColour(SDL_Colour & fg, SDL_Colour & bg) const;

    virtual void GetColourAtLoc(int addr, SDL_Colour & fg, SDL_Colour & bg);
};

/////////////////////////////////////////////////////////////////////////////////

class ColourMemoryMappedScreen : public MemoryMappedScreen
{
  public:
    ColourMemoryMappedScreen(MainWindow & mainWindow, const Options & options, const Config::MemoryMappedScreen & info);

    virtual void SetFontColour(const SDL_Colour & fg, const SDL_Colour & bg);
    virtual void GetFontColour(SDL_Colour & fg, SDL_Colour & bg) const;
};

/////////////////////////////////////////////////////////////////////////////////

#endif // VIRTUALSCREEN_H_

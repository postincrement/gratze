
#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <memory>
#include <chrono>

#include <SDL.h>

#include "video/virtual_screen.h"

#define LAZY_UPDATE_MSECS   20

class MainWindow
{
  public:
    MainWindow();
    ~MainWindow();

    bool Open(int width, int height);

    SDL_Renderer * GetRenderer();

    void Update();

    void GetScreenCharRect(SDL_Rect & rect, int x, int y, int w, int h, int hscale, int vscale);

  protected:
    int m_screenHeight;
    int m_screenWidth;
    int m_leftBorder = 10;
    int m_topBorder = 10;

    std::string m_title;

    SDL_Window * m_window;
    SDL_Renderer * m_renderer;
    SDL_Texture * m_screenTexture;

    // virtual video screen
    SDL_Rect  m_screenRect;
    std::unique_ptr<VirtualScreen> m_screen;
};

#endif // MAINWINDOW_H_

#ifndef MODEL4_H_
#define MODEL4_H_

#include "z80/trs80/trs80.h"

class Model4_Emulator : public TRS80Emulator
{
  public:
    Model4_Emulator();
};

#endif // MODEL4_H_
#ifndef MODEL3_H_
#define MODEL3_H_

#include "z80/trs80/trs80.h"
#include "z80/trs80/model1/model1.h"

class Model3_Emulator : public Model1_Emulator
{
  public:
    Model3_Emulator();
};

#endif // MODEL3_H_


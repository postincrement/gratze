
#ifndef VIRTUAL_DRIVE_DMK_H_
#define VIRTUAL_DRIVE_DMK_H_

#include <stdint.h>
#include <string>
#include <vector>

#include "virtual_drive.h"

class VirtualDriveDMK : public VirtualDriveFile
{
  public:
    VirtualDriveDMK();
    virtual bool OpenFile(int fd, off_t len, const uint8_t * header, size_t headerSize) override;
};

#endif // VIRTUAL_DRIVE_DMK_H_